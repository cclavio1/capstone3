import {  useEffect, useState } from "react"
import {Card,Button} from 'react-bootstrap'
import { useNavigate } from "react-router-dom"
import AppNavBar from "../components/AppNavBar"

export default function ProductPage(){
    let id = window.location.search.slice(4)
    let [product,setProduct]=useState()
    let navigate = useNavigate()
    let [quantity,setQuantity]=useState(1);

    if(localStorage.getItem('isAdmin')=='true'){
            navigate(`/editProduct?id=${id}`)
    }
    useEffect(()=>{
        
        fetch(`https://thawing-hamlet-00090.herokuapp.com/api/products/${id}/productInfo`,{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result=>result.json())
        .then(result=>{
            setProduct(result.values)
        })
    },[])
    useEffect(()=>{
        if(quantity<=0){
            alert("Cannot reduce quantity")
            setQuantity(++quantity)
        }
    })
    function addtocart(){
        if(product._id==undefined){
            alert('please wait as we load the page')
        }else{
            let arr = localStorage.getItem('cart').trim().split(" ")
            if(arr.includes(product._id)){
                alert('This product is already on your cart')
                navigate('/landingPage')
            }else{
                localStorage.setItem('cart',localStorage.getItem('cart')+" "+product._id)
                localStorage.setItem('cartQ',localStorage.getItem('cartQ')+" "+quantity)
                alert('Successfully added on cart!')
                navigate('/landingPage')
            }
        }
    }
    let render = 
        <div className="text-center">
            <img src="https://c.tenor.com/I6kN-6X7nhAAAAAj/loading-buffering.gif"></img>
        </div>

    if(product!=undefined){
        render = 
        <Card className="my-5 text-center">
        <Card.Img variant="top" src={product.imgurl} className="img-fluid col-2" style={{maxWidth:"350px", margin:"auto"}} />
        <div className="row">
            <Card.Body className='col-8'>
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>
                {product.description}
                </Card.Text>
                <p>{product.buycount} people have already bought this product</p>
                <Card.Text>
                Price: &#8369;<span className="text-warning">{product.price}</span>
                </Card.Text>
                <Card.Text>
                Quantity:
                </Card.Text>
                <div className="m-2 border border-dark d-flex justify-content-between mx-auto" style={{width:"100px"}}>
                    <Button variant="danger" onClick={()=>setQuantity(--quantity)}>-</Button>
                    <label>{quantity}</label>
                    <Button variant="success" onClick={()=>setQuantity(++quantity)}>+</Button>
                </div>
                <div className="d-grid gap-2">
                    <Button variant="success" onClick={addtocart}>Add To Cart</Button>
                    <Button variant="warning" onClick={()=>navigate('/landingPage')}>Continue Browsing</Button>
                </div>
            </Card.Body>
        </div>
    </Card>
        
    }

    return(
        <div>
            <AppNavBar/>
            {render}
        </div>
    )
}