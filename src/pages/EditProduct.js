import { useEffect, useState } from "react"
import { Card ,Button,Form, Badge} from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import AppNavBar from "../components/AppNavBar"


export default function EditProduct(){
    let navigate = useNavigate()
    let prodid=window.location.search.slice(4)
    let [Pimgurl,setImgurl]=useState('')
    let [Pname,setName]= useState('')
    let [Pdescription,setDescription]=useState('')
    let [Pprice,setPrice]=useState(0)
    let [PisActive,setIsActive]=useState(true)
    let[Pid,setId]=useState('')
    let [Ptag,setTag]=useState([])
    let [inputTag,setInputTag]=useState('')
    let [tagsList,setTagsList]=useState(<p className="d-inline">None</p>)
    let [disableInputTag,setDisableInputTag]=useState(true)

    useEffect(()=>{
        if(localStorage.getItem('token')==null||localStorage.getItem('isAdmin')!='true'){
            navigate('/')
        }
        fetch(`https://thawing-hamlet-00090.herokuapp.com/api/products/${prodid}/productInfo`,{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        }).then(result=>result.json())
        .then(result=>{
            let{imgurl,name,description,price,isActive,_id,tags}=result.values
            setImgurl(Pimgurl=imgurl)
            setName(Pname=name)
            setDescription(Pdescription=description)
            setPrice(Pprice=price)
            setIsActive(PisActive=isActive)
            setId(Pid=_id)
            if(tags.length!=0){
                setTag(Ptag=tags)
                updateTags()
            }
        })
    },[])
    function applyChanges(){
        if(Pprice<=0){
            alert('Price cannot be 0 nor negative')
        }else{
            fetch(`https://thawing-hamlet-00090.herokuapp.com/api/products/${Pid}/update`,{
                method:"PATCH",
                headers:{
                    "Authorization":`Bearer ${localStorage.getItem('token')}`,
                    "Content-type":'application/json'
                },
                body:JSON.stringify({
                    name:Pname,
                    description:Pdescription,
                    price:Pprice,
                    imgurl:Pimgurl,
                    isActive:PisActive
                })
            }).then(result=>result.json(),err=>alert(err))
            .then(result=>{
                fetch(`https://thawing-hamlet-00090.herokuapp.com/api/products/addTag/${Pid}`,{
                    method:"PATCH",
                    headers:{
                        "Content-type":"application/json"
                    },
                    body:JSON.stringify({
                        tag:Ptag
                    })
                }).then(()=>{
                    alert('Changes have been saved')
                    navigate('/landingPage')
                })
            })
        }
    }
    
    useEffect(()=>{
        if(inputTag!=""){
            setDisableInputTag(false)
        }else{
            setDisableInputTag(true)
        }
    },[inputTag])
    
    function updateTags(){
        if(Ptag!=[]){
            setTagsList(Ptag.map(tag=>{
                return(
                    <Badge 
                        key={tag} className="mx-1">{tag} 
                        <a className="link nav-link d-inline link-dark"
                        style={{paddingLeft:"5px",paddingRight:"0px"}}
                        onClick={()=>removeTag(tag)}
                        >x</a>
                    </Badge>
                )
            }))
        }else{
            setTagsList(<p className="d-inline">None</p>)
        }
    }
    function removeTag(tag){
     let arr = Ptag
     let indexToRemove = Ptag.indexOf(tag)   
     arr.splice(indexToRemove,1)
     setTag(arr)

     setInputTag(inputTag="")
     updateTags()
    }
    function addTag(){
        let arr = Ptag
        arr.push(inputTag)
        setTag(arr)
        setInputTag(inputTag="")
        updateTags()
    }

    return(
        <div>
            <AppNavBar isAdmin={'true'}/>
        <Card className="my-5 text-center"> 
            
        <Card.Img variant="top" src={Pimgurl} className="img-fluid col-2" style={{maxWidth:"350px", margin:"auto"}} />
                    <Form className="w-50 mx-auto">
            <Form.Group className="mb-3">
                <Form.Label>Image Url</Form.Label>
                <Form.Control type="text" value={Pimgurl} onChange={(e)=>setImgurl(Pimgurl=e.target.value)}/>
			</Form.Group>
            <Form.Group className="mb-3">
                <Form.Label>Product Name</Form.Label>
                <Form.Control type="text" value={Pname} onChange={(e)=>setName(Pname=e.target.value)}/>
			</Form.Group>
            <Form.Group className="mb-3">
                <Form.Label>Product Description</Form.Label>
                <Form.Control type="text" value={Pdescription} onChange={(e)=>setDescription(Pdescription=e.target.value)}/>
			</Form.Group>
            <Form.Group className="mb-3 text-start">
                <Form.Label>Tags: {tagsList}</Form.Label>
                <div className="row">
                    <Form.Control type="text" placeholder="Ex. Electronics, Food, etc,." className="col-6" onChange={(e)=>setInputTag(e.target.value)} value={inputTag}/>
                    <Button disabled={disableInputTag} variant="primary" className="col-6" onClick={()=>addTag()}>Add Tag</Button>
                </div>
                
			</Form.Group>


            <Form.Group className="mb-3">
                <Form.Label>Price</Form.Label>
                <Form.Control type="number" value={Pprice} onChange={(e)=>setPrice(Pprice=e.target.value)}/>
			</Form.Group>
            <Form.Check 
                type="switch"
                id="custom-switch"
                label="On Sale"
                onChange={(e)=>setIsActive(PisActive=e.target.checked)}
                checked={PisActive}
            />
            <div className="d-grid gap-2">
                        <Button variant="success" onClick={applyChanges}>Apply Changes</Button>
            </div>
            </Form>
           
        </Card>
        </div>
    )
}