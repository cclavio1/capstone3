import Product from "../components/Product";
import {useState,useEffect} from "react"
import AppNavBar from "../components/AppNavBar";
import { useNavigate } from "react-router-dom";
import Footer from "../components/Footer";
import ControlledCarousel from "../components/TopProducts";
import { Form,Button  } from "react-bootstrap";

export default function LandingPage(){
    let navigate=useNavigate()
    let cart = []
    let localCart=localStorage.getItem('cart')
    let cartQ = []
    let [products,setProducts]= useState([])
    let [search,setSearch]= useState('')
    let [render,setRender]=useState(<div className="text-center">
    <img src="https://c.tenor.com/I6kN-6X7nhAAAAAj/loading-buffering.gif"></img>
</div>);
    

    useEffect(()=>{
        if(localStorage.getItem('token')==null){
            navigate('/')
        }
        if(localStorage.getItem('isAdmin')=='true'){
            fetch("https://thawing-hamlet-00090.herokuapp.com/api/products/")
            .then(result=>result.json())
            .then(result=>{
                setProducts(products=result)
                if(products.length!=0){
                    renderThis(products)
                }
            })
        }else{
            fetch("https://thawing-hamlet-00090.herokuapp.com/api/products/activeProducts")
            .then(result=>result.json())
            .then(result=>{
                setProducts(products=result)
                if(products.length!=0){
                    renderThis(products)
                }
            })
            if(localCart==null||localCart==''){
                localStorage.setItem('cart',"")
                localStorage.setItem('cartQ',"")
            }else{
                cart = localStorage.getItem('cart').trim().split(" ")
                cartQ = localStorage.getItem('cartQ').trim().split(" ")
            }
        }
        },[])
        function go(){
            if(search==""){
                renderThis(products)
            }else{
                filterRender(products)
            }
        }
        function filterRender(prods){
            let newArr= prods.filter(data=>data.tags.includes(search.toLowerCase()))
            let nameSearchArr=
            prods.filter(data=>data.name.match(RegExp(search,'i')))
            nameSearchArr.forEach(data=>{
                if(!newArr.includes(data)){
                    newArr.push(data)
                }
            })

            renderThis(render=newArr)
        }
        function renderThis(productsToDisplay){
            if(productsToDisplay.length==0){
                setRender(<div className="row text-center bg-secondary container"> <h3>No Result Found</h3></div>)
            }else{
                setRender(productsToDisplay.map(data=><Product key={data._id} productProp={data} />))
            }
        }
    return(
            <div style={{backgroundImage:'url(https://wallpaperaccess.com/full/1624843.jpg)'}}>
                <AppNavBar isAdmin={localStorage.getItem('isAdmin')}/>
                <h3 className="bg-dark text-light text-center m-0 p-2">Top Products</h3>
                <ControlledCarousel />
                <div className="container"> 
                    <Form className="row">
                        <Form.Control type="text" placeholder="Search by name or tags...."
                         className="col text-center" value={search} onChange={(e)=>setSearch(e.target.value)}/>
                        <Button className="col-2" onClick={()=>go()}>Go</Button>
                    </Form></div>
                <div className="row justify-content-center">
                    {render}   
                </div>
                <Footer />
            </div>
    )

}