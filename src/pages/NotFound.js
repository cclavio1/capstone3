

export default function NotFound(){
	return(
		<div>
		<h1>404 Page Cannot be found</h1>
		<h4>Click <a href="/">here</a> to go back</h4>
		</div>
	)
}