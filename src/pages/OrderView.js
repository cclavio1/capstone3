import { useEffect, useState } from "react"
import { Card } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import AppNavBar from "../components/AppNavBar"
import Product from "../components/Product"

export default function OrderView(){
    let navigate= useNavigate()
    let id  = window.location.search.slice(4)
    let [order,setOrder] = useState()
    let [products,setProducts]=useState([])
    let render;
    let [renderProds,setRenderProds]=useState(<img className="w-25 m-auto" src="https://c.tenor.com/I6kN-6X7nhAAAAAj/loading-buffering.gif"></img>)
    useEffect(()=>{
        fetch(`https://thawing-hamlet-00090.herokuapp.com/api/orders/${id}`,{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        }).then(result=>result.json())
        .then(result=>{
            setOrder(result)
            setProducts(products=result.products)
        })
        setTimeout(()=>delay(),2000)
    },[])
    
if(order!=undefined){
    render=          
    <Card className="m-2 text-center">
        <a className="btn btn-outline-primary" href=""onClick={()=>{
            if(localStorage.getItem('isAdmin')=='true'){navigate('/allOrders')}
            else{navigate('/orders')}}
            }>Back</a>
        <Card.Body className="mx-auto">
             <Card.Title>Order Details</Card.Title>
             <Card.Text>ID: {order._id}</Card.Text>
             <Card.Text>Owner: {order.emailOfCX}</Card.Text>
             <Card.Text>Date of Purchase: {order.purchasedOn.slice(0,10)}</Card.Text>
             <Card.Text>Total Amount: &#8369; {order.totalAmount}</Card.Text>
             <Card.Text>Items:</Card.Text>
        {renderProds}
        </Card.Body>
    </Card>
}
function delay(){
    let prodids= products.map(id=>{
        return id.productId
    })
    let newProds
    fetch('https://thawing-hamlet-00090.herokuapp.com/api/products/productsInfos',{
        method:"POST",
        headers:{
            "Content-type":"application/json"
        },
        body:JSON.stringify({
            prods:prodids
        })
    }).then(result=>result.json())
    .then(result=>{
        newProds=result
    })
    setTimeout(()=>{
        setRenderProds(newProds.map(prod=>{
            if(prod==null){
                return(
                    <div key={"noID"} className="bg-warning py-2 mx-auto" style={{width:'350px'}}>
                            <h2>This product has been removed by the administrator</h2>
                    </div>
                    )
            }else{
                return(
                <Product className="text-center m-auto" key={prod._id} productProp={prod}/>
                )
            }
            
        }))
    },2000)
}
    return(
        <div>
            <AppNavBar isAdmin={localStorage.getItem('isAdmin')}/>
            {render}
        </div>
    )
}