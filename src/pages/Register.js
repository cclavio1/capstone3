import {Form,Button} from 'react-bootstrap'
import {useState,useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import Banner from '../components/Banner'

export default function Login(){
	const [email,setEmail]=useState('')
	const [pw,setPW] = useState('')
    const [cPW,setCPW] = useState('')
	const [isDisabled,setIsDisabled] = useState(true)
	const navigate = useNavigate()

	useEffect(()=>{
if(localStorage.getItem('token')!==null){
	navigate('/landingPage')
}
		if(email==""||pw==""||cPW==""){
			setIsDisabled(true)
		}else{
			setIsDisabled(false)
		}
	})
	let login=(e)=>{
        e.preventDefault()
        if(pw==cPW){
            fetch('https://thawing-hamlet-00090.herokuapp.com/api/users/register',{
                method:"POST",
                headers:{
                    "Content-type":"application/json"
                },
                body:JSON.stringify({
                    email:email,
                    password:pw
                })
            }).then(result=>result.json(),err=>alert("Server is Down please try again later"))
            .then(result=>{
                if(result.status=="error"){
                    alert(result.error)
                }else{
                    alert("Successful Registration")

                    navigate('/')
                }
            })
        }else{
            alert('Password does not match')
        }
	}

	return(
		<div>
			<Banner/>
			<div className='text-center'>
			<h6>Already have an account? <a href='' onClick={()=>navigate('/')}>Login</a> instead</h6>
			</div>
			<Form className=" bg-info p-3 mx-5 text-center mx-auto" style={{maxWidth:"350px"}}>
			  <Form.Group className="mb-3">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" value={email} onChange={(e)=>setEmail(e.target.value)}/>
			  </Form.Group>

			  <Form.Group className="mb-3">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" value={pw} onChange={(e)=>setPW(e.target.value)}/>
			  </Form.Group>

              <Form.Group className="mb-3">
			    <Form.Label>Confirm Password</Form.Label>
			    <Form.Control type="password" value={cPW} onChange={(e)=>setCPW(e.target.value)}/>
			  </Form.Group>

			  <Button variant="primary" disabled={isDisabled} type="submit" onClick={login}>
			    Submit
			  </Button>
			</Form>
			
		</div>
		)
}