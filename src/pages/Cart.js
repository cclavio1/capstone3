import { useContext, useEffect, useState } from "react"
import { Col,Row, Container,Button} from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import AppNavBar from "../components/AppNavBar"
import CartItem from "../components/CartItem"
import { CartProvider } from "../pages/CartContext"

export default function Cart(){
    
    let [prods,setProds]=useState([])
    let ids=localStorage.getItem('cart').trim().split(' ')
    let quans = localStorage.getItem('cartQ').trim().split(' ')
    let navigate = useNavigate()
    localStorage.setItem('price',"")
    
    
    useEffect(()=>{
        if(localStorage.getItem('cart')==""){
            alert('your cart is empty, Feel free to Browse our items')
            navigate('/landingPage')
        }else{
            fetch('https://thawing-hamlet-00090.herokuapp.com/api/products/productsInfos',{
                        method:"POST",
                        headers:{
                            "Content-type":"application/json"
                        },
                        body:JSON.stringify({
                            prods:ids
                        })
                    }).then(result=>result.json())
                    .then(result=>{
                        setProds(result)
                    })
            }
        },[])
        let newProds= []
        let [total,setTotal]=useState(0)

        for(let x=0;x<prods.length;x++)
        {
            newProds[x]={
                id:ids[x],
                name:prods[x].name,
                price:prods[x].price,
                quantity:quans[x]
            }
            localStorage.setItem('price',localStorage.getItem('price')+" "+newProds[x].price*newProds[x].quantity)
        }
  
        let render=newProds.map(product=>{
            return<CartItem key={product.id} prodProp={product}/>
        })
        
        function checkOut(){
            let prodid = localStorage.getItem('cart').trim().split(' ')
            let proquan= localStorage.getItem('cartQ').trim().split(' ')
            let toCheckOut=[]
            let addBuyCount=[]
            for(let x =0;x<prodid.length;x++){
                toCheckOut.push({
                    productId:prodid[x],
                    quantity:proquan[x]
                })
                addBuyCount.push({
                    id:prodid[x],
                    quantity:Number(proquan[x])
                })
            }
            let order={
                totalAmount:total,
                products:toCheckOut
            }
            fetch('https://thawing-hamlet-00090.herokuapp.com/api/orders/create',{
                method:"POST",
                headers:{
                    "Authorization":`Bearer ${localStorage.getItem('token')}`,
                    "Content-type":"application/json"
                },
                body:JSON.stringify(order)
            }).then(result=>result.json())
            .then(result=>{
                localStorage.setItem('cart',"")
                localStorage.setItem('cartQ',"")
                localStorage.setItem('price',"")
                fetch('https://thawing-hamlet-00090.herokuapp.com/api/products/updateBuyCount',{
                    method:'PATCH',
                    headers:{
                        "Content-type":"application/json"
                    },
                    body:JSON.stringify({
                        prods:addBuyCount
                    })
                })
                alert('Order has been posted')
                navigate('/orders')
            })
        }
    return(
        <div>
            <AppNavBar/>
            <Container className="align-items-center">
                <h4 className="text-center m-4">Your Cart</h4>
                <Row className="bg-dark text-light">
                    <Col>
                        <h6>Product Name</h6>
                    </Col>
                    <Col className="text-center">
                        <h6>Price: </h6>
                    </Col>
                    <Col className="text-center">
                         Quantity:
                    </Col>
                    <Col>
                    </Col>
                </Row>
                <CartProvider value={{total,setTotal}}> {render}</CartProvider>
                    <Row>
                        <Col xs={8}>
                             <div className="d-grid gap-2" onClick={checkOut}><Button>Checkout</Button></div>
                        </Col>
                        <Col>Total: &#8369; <span className="text-warning" id={total}>{total}</span> </Col>
                    </Row>
            </Container>
        </div>
    )
}
