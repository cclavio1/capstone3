import { useEffect, useState } from "react"
import { Card, Form, Button, Badge } from "react-bootstrap"
import { useNavigate } from "react-router-dom"
import AppNavBar from "../components/AppNavBar"


export default function AddProduct(){
    let navigate = useNavigate()
    let [Pimgurl,setImgurl]=useState('https://cdn-icons-png.flaticon.com/512/3724/3724788.png')
    let [Pname,setName]= useState('')
    let [Pdescription,setDescription]=useState('')
    let [Pprice,setPrice]=useState(0)
    let [tagsList,setTagsList]=useState(<p className="d-inline">None</p>)
    let [inputTag,setInputTag]=useState('')
    let [disableInputTag, setDisableInputTag]=useState(true)
    let [Ptag,setTag]=useState([])

    function save(){
        if(Pprice<=0){
            alert('Price is invalid')
        }else{
            if(Pimgurl.trim()==""){
                setImgurl(Pimgurl='https://cdn-icons-png.flaticon.com/512/3724/3724788.png')
            }
            if(Pname.trim()==""||Pdescription.trim()==""){
                alert('Please complete the details to create a new product')
            }else{
                fetch('https://thawing-hamlet-00090.herokuapp.com/api/products/create',{
                    method:"POST",
                    headers:{
                        "Authorization":`Bearer ${localStorage.getItem('token')}`,
                        "Content-type":"application/json"
                    },
                    body:JSON.stringify({
                        name:Pname.trim(),
                        description:Pdescription.trim(),
                        price:Pprice,
                        imgurl:Pimgurl
                    })
                }).then(result=>result.json())
                .then(result=>{
                    fetch(`https://thawing-hamlet-00090.herokuapp.com/api/products/addTag/${result._id}`,{
                    method:"PATCH",
                    headers:{
                        "Content-type":"application/json"
                    },
                    body:JSON.stringify({
                        tag:Ptag
                    })
                    }).then(()=>{
                        alert('Product has been created')
                        navigate('/landingPage')
                    })
                })
            }
        }
    }
    function addTag(){
        let arr = Ptag
        arr.push(inputTag)
        setTag(arr)
        setInputTag(inputTag="")
        updateTags()
    }
    function removeTag(tag){
        let arr = Ptag
        let indexToRemove = Ptag.indexOf(tag)   
        arr.splice(indexToRemove,1)
        setTag(arr)

        setInputTag(inputTag="")
        updateTags()
    }
    function updateTags(){
        if(Ptag!=[]){
            setTagsList(Ptag.map(tag=>{
                return(
                    <Badge 
                        key={tag} className="mx-1">{tag} 
                        <a className="link nav-link d-inline link-dark"
                        style={{paddingLeft:"5px",paddingRight:"0px"}}
                        onClick={()=>removeTag(tag)}
                        >x</a>
                    </Badge>
                )
            }))
        }else{
            setTagsList(<p className="d-inline">None</p>)
        }
    }

    useEffect(()=>{
        if(inputTag!=""){
            setDisableInputTag(false)
        }else{
            setDisableInputTag(true)
        }
    },[inputTag])
    return(
    <div>
        <AppNavBar isAdmin={'true'}/>
    <Card className="my-5 text-center"> 
        
    <Card.Img variant="top" src={Pimgurl} className="img-fluid col-2" style={{maxWidth:"100px",maxHeight:"100px", margin:"auto"}} />
                <Form className="w-50 mx-auto">
        <Form.Group className="mb-3">
            <Form.Label>Image Url</Form.Label>
            <Form.Control type="text" value={Pimgurl} onChange={(e)=>setImgurl(Pimgurl=e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>Product Name</Form.Label>
            <Form.Control type="text" value={Pname} onChange={(e)=>setName(Pname=e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>Product Description</Form.Label>
            <Form.Control type="text" value={Pdescription} onChange={(e)=>setDescription(Pdescription=e.target.value)}/>
        </Form.Group>

        <Form.Group className="mb-3 text-start">
                <Form.Label>Tags: {tagsList}</Form.Label>
                <div className="row">
                    <Form.Control type="text" placeholder="Ex. Electronics, Food, etc,." className="col-6" onChange={(e)=>setInputTag(e.target.value)} value={inputTag}/>
                    <Button disabled={disableInputTag} variant="primary" className="col-6" onClick={()=>addTag()}>Add Tag</Button>
                </div>
                
			</Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" value={Pprice} onChange={(e)=>setPrice(Pprice=e.target.value)}/>
        </Form.Group>

        <div className="d-grid gap-2">
                    <Button variant="success" onClick={save}>Save Product</Button>
        </div>
        </Form>
       
    </Card>
    </div>
    )
}