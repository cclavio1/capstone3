import { useEffect, useState } from "react"
import { Col, Row } from "react-bootstrap"
import AppNavBar from "../components/AppNavBar"
import Order from "../components/Order"

export default function AllOrders(){
    let [orders,setOrders]= useState([])
    useEffect(()=>{
        fetch('https://thawing-hamlet-00090.herokuapp.com/api/orders/allOrders',{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        }).then(result=>result.json())
        .then(result=>{
            setOrders(orders=result)
        })
    },[])
    let render= orders.map(data=>{
        return<Order key={data._id} orderProp={data}/>
    })
    return(
        <div>
            <AppNavBar isAdmin={'true'}/>
            <h2 className="text-center m-4">All Orders</h2>
            <Row className="bg-dark text-light" style={{minWidth:"750px"}}>
                <Col xs={4}>
                    <h6>Order ID</h6>
                </Col> 
                <Col xs={3}>
                    <h6>Owner</h6>
                </Col>
                <Col xs={2} className="text-center">
                    <h6>Total Amount: </h6>
                </Col>
                <Col className="text-center">
                     Date of purchase:
                </Col>
            </Row>
            <Row>
                {render}
            </Row>
        </div>
    )
}