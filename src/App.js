import Login from "./pages/Login"
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import NotFound from "./pages/NotFound"
import Register from "./pages/Register"
import LandingPage from "./pages/LandingPage"
import ProductPage from "./pages/ProductPage"
import Cart from "./pages/Cart"
import Orders from "./pages/Orders"
import EditProduct from './pages/EditProduct'
import AllOrders from "./pages/AllOrders"
import AddProduct from "./pages/AddProduct"
import OrderView from "./pages/OrderView"


function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login/>}/>
        <Route path="/register" element={<Register/>}/>
        <Route path="/landingPage" element={<LandingPage/>}/>
        <Route path="/productPage" element={<ProductPage/>}/>
        <Route path="/cart" element={<Cart/>}/>
        <Route path="/orders" element={<Orders/>}/>
        <Route path="/editProduct" element={<EditProduct/>}/>
        <Route path="/allOrders" element={<AllOrders/>}/>
        <Route path="/addProduct" element={<AddProduct/>}/>
        <Route path="/orderView" element={<OrderView/>}/>
        <Route path="*" element={<NotFound />}/>
      </Routes>
    </BrowserRouter>
    
  );
}

export default App;
