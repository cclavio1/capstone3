
import { useContext, useEffect, useState } from "react"
import { Col,Row, Button} from "react-bootstrap"
import CartContext from "../pages/CartContext"


export default function CartItem({prodProp}){
            let [quantity,setQuantity]=useState(prodProp.quantity)
            let [price,setPrice]=useState(prodProp.price*quantity)
            let {total,setTotal}=useContext(CartContext)
            
            useEffect(()=>{
                if(quantity<=0){
                    deleteItem()
                }else{
                
                }
                calcTotal()
            },[price])
            function deleteItem(){
                let ids = localStorage.getItem('cart').trim().split(' ')
                let quans = localStorage.getItem('cartQ').trim().split(' ')
                let index =ids.indexOf(prodProp.id)
                ids.splice(index,1)
                quans.splice(index,1)
                localStorage.setItem('cart',ids.join(" "))
                localStorage.setItem('cartQ',quans.join(" "))
                window.location.reload();
            }
            function decQuan(){
                setQuantity(--quantity)
                setPrice(price=prodProp.price*quantity)
                let ids = localStorage.getItem('cart').trim().split(' ')
                let quans = localStorage.getItem('cartQ').trim().split(' ')
                let subtotals = localStorage.getItem('price').trim().split(' ')
                let index =ids.indexOf(prodProp.id)
                quans[index]=quantity
                subtotals[index]=price
                localStorage.setItem('cartQ',quans.join(" "))
                localStorage.setItem('price',subtotals.join(" "))
                calcTotal()
            }
            function addQuan(){
                setQuantity(++quantity)
                setPrice(price=prodProp.price*quantity)
                let ids = localStorage.getItem('cart').trim().split(' ')
                let quans = localStorage.getItem('cartQ').trim().split(' ')
                let subtotals = localStorage.getItem('price').trim().split(' ')
                let index =ids.indexOf(prodProp.id)
                quans[index]=quantity
                subtotals[index]=price
                localStorage.setItem('cartQ',quans.join(" "))
                localStorage.setItem('price',subtotals.join(" "))
                calcTotal()
            }
            function calcTotal(){
               let priceArr = localStorage.getItem('price').trim().split(' ')
               setTotal(priceArr.reduce((a,b)=>Number(a)+Number(b)))

            }
    return(
        <Row className="align-items-center">
                <Col>{prodProp.name}</Col>
                <Col className="text-center" >&#8369; <span className="text-warning">{price}</span></Col>
                <Col className="m-2 border border-dark d-flex justify-content-between mx-auto" style={{width:"100px"}}>
                <Button variant="danger" onClick={decQuan}>-</Button>
                    <label>{quantity}</label>
                <Button variant="success" onClick={addQuan}>+</Button>
                </Col>
                <Col className="text-center">
                    <Button variant="danger" onClick={deleteItem}>Remove Item</Button>
                </Col>
        </Row>
)
}