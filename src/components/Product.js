import {Card, Button, Badge} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

export default function Product({productProp}){
    const navigate = useNavigate()
    let render;        
    let tagsList;
    
    if(productProp.tags.length!=0){
        tagsList=productProp.tags.map(tag=>{
            return(<Badge className='mx-1' key={tag}>{tag}</Badge>)
        })
    }else{
        tagsList="no tags"
    }
    if(localStorage.getItem('isAdmin')=="true"){
        let border="m-2";
        if(!productProp.isActive){
            border +=" border-5 border-danger"
        }
        render=
        <Card style={{ width: '25rem', height:"10rem"}} className={border}>
            <div className="row">
                <Card.Img variant="top" src={productProp.imgurl} className="img-fluid col-2" style={{maxWidth:"100px",maxHeight:"100px", margin:"auto"}} />
                <Card.Body className='col-8'>
                    <Card.Title>{productProp.name}</Card.Title>
                    <a className='btn btn-outline-success' onClick={()=>navigate(`/editProduct?id=${productProp._id}`)}>Edit Product</a>
                </Card.Body>
                <div className='text-end'>{tagsList}</div>
            </div>
        </Card>
    }else{
        render=
            <Card style={{ width: '25rem', height:"10rem"}} className="m-2">
                <div className="row">
                    <Card.Img variant="top" src={productProp.imgurl} className="img-fluid col-2" style={{maxWidth:"100px",maxHeight:"100px", margin:"auto"}} />
                    <Card.Body className='col-8'>
                        <Card.Title>{productProp.name}</Card.Title>
                        <Button variant="primary" onClick={()=>navigate(`/productPage?id=${productProp._id}`)}>Check Details</Button>
                    </Card.Body>
                    <span className='text-end px-4 text-secondary'>{productProp.buycount} sold</span>
                    <div className='text-end'>{tagsList}</div>
                    
                </div>
            </Card>
    }
    return render
}