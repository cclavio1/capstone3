

export default function Banner(){
    return(
        <div className="text-center bg-warning" style={{marginBottom:'5vh'}}><h2 className="d-inline-flex" style={{
            fontSize:'5rem',
            fontFamily:"Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif"
        }}>Shopree</h2><span><img className="img-fluid w-25" src="https://cdn-icons-png.flaticon.com/512/2331/2331966.png"/></span></div>
    )
}