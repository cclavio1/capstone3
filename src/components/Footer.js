import logo from '../courierCat2.png'


export default function Footer(){
    return(
        <div className='text-center text-white my-3' style={{fontSize:"0.7rem"}}>
            <div className='row'>
                <div className='col-6'>
                    <p className='d-inline-flex bg-dark'>Need help with our products?</p><br/>
                    <p className='d-inline-flex bg-dark'>You can call our customer service hotline @ +123 456 7891</p><br/>
                    <p className='d-inline-flex bg-dark'>This cat is in charge with the delivery</p><br/>
                    <p className='d-inline-flex bg-dark'>You can reach out on any of the following media:</p><br/>
                    <div className='d-flex justify-content-evenly'>
                        <a href="https://www.facebook.com/4dobo-Gaming-108798247959176/" target="_blank"><img src='https://img.icons8.com/color/344/facebook-new.png' style={{width:"50px"}}/></a>
			        	<a href="https://www.youtube.com/channel/UCB63XWXM_Tc2TQ8haxwo_RA" target="_blank"><img src='https://img.icons8.com/fluency/344/youtube-play.png'style={{width:"50px"}}/></a>
                    </div>
                </div>
                <div className='col-5'>
                    <img src={logo} className="img-fluid"></img>
                </div>
            </div>
            <div className='text-center p-2 bg-dark text-white'>All rights reserved @ 2022</div>
        </div>
    )
}