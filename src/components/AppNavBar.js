import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

export default function AppNavBar({isAdmin}){
    let navigate = useNavigate()

    function logout(){
        localStorage.clear()
        alert('Thank you for shopping with us')
        navigate('/')
    }
    let render;
    if(isAdmin=="true"){
        render =<div className="d-flex justify-content-start align-items-center bg-info sticky-top">
        <img src='https://cdn-icons-png.flaticon.com/512/2331/2331966.png' style={{width:'50px'}} alt="SHOPREE"/><h2>SHOPREE</h2>
        
        <ul className="nav d-flex justify-content-between" style={{marginLeft:"auto"}}>
            <ol className="nav-item"><a className="nav-link" href="/landingPage">Browse</a></ol>
            <ol className="nav-item"><a className="nav-link" href="/allOrders">View All Orders</a></ol>
            <ol className="nav-item"><a className="nav-link" href="/addProduct">Add Product</a></ol>
            <ol className="nav-item"><a className="nav-link" href="" onClick={logout}>Logout</a></ol>
        </ul>
    </div>
    }else{
        let cartItemQuantity = 0
        if(localStorage.getItem('cart')!=null){
            if(!localStorage.getItem('cart').trim().split(" ")[0]=='')
            {
            cartItemQuantity=localStorage.getItem('cart').trim().split(' ').length
            }
        }
        render =<div className="d-flex justify-content-start align-items-center bg-info sticky-top">
        <img src='https://cdn-icons-png.flaticon.com/512/2331/2331966.png' style={{width:'50px'}} alt="SHOPREE"/><h2>SHOPREE</h2>
        <ul className="nav d-flex justify-content-between" style={{marginLeft:"auto"}}>
            <ol className="nav-item"><a className="nav-link" href="/landingPage">Browse</a></ol>
            <ol className="nav-item"><a className="nav-link" href="/cart">Cart -{cartItemQuantity}</a></ol>
            <ol className="nav-item"><a className="nav-link" href="/orders">My Orders</a></ol>
            <ol className="nav-item"><a className="nav-link" href="" onClick={logout}>Logout</a></ol>
        </ul>
    </div>
    }
    useEffect(()=>{
        //check token redirect to logout if not valid
        fetch(`https://thawing-hamlet-00090.herokuapp.com/api/users/checkToken`,{
            headers:{
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            }
        }).then(result=>result.json()).then(result=>{
            if(result.status=="error"){
                alert('Your session has expired, please log in again')
                localStorage.clear()
                navigate('/')
            }
        })
    },[])
    return render
}