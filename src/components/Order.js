import { Col, Row } from "react-bootstrap"


export default function Order({orderProp}){
    let render
    if(localStorage.getItem('isAdmin')=='true')
    {
        render=        
        <Row className="align-items-center" style={{minWidth:"750px"}}>
        <Col xs={4}><a href={`/orderView?id=${orderProp._id}`}>{orderProp._id}</a></Col>
        <Col xs={3}>{orderProp.emailOfCX}</Col>
        <Col xs={2}>&#8369; <span className="text-warning">{orderProp.totalAmount}</span></Col>
        <Col className="text-center">{orderProp.purchasedOn.slice(0,10)}</Col>
        </Row>
    }else{
        render=
        <Row className="align-items-center" style={{minWidth:"750px"}}>
        <Col xs={4}><a href={`/orderView?id=${orderProp._id}`}>{orderProp._id}</a></Col>
        <Col xs={2} className="text-center" >&#8369; <span className="text-warning">{orderProp.totalAmount}</span></Col>
        <Col className="text-center">{orderProp.purchasedOn.slice(0,10)}</Col>
        <Col></Col>
        </Row>
    }
    return render
    
}