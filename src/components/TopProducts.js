import { Carousel } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function ControlledCarousel() {
    const [index, setIndex] = useState(0);
    const navigate = useNavigate()
    const [topProds,setTopProds] = useState()
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    }

    useEffect(()=>{
        fetch('https://thawing-hamlet-00090.herokuapp.com/api/products/topProds').then(result=>result.json()).then(result=>{
            setTopProds(result)
        })
    },[])
let render=<div className="text-center">
    <img src="https://c.tenor.com/I6kN-6X7nhAAAAAj/loading-buffering.gif"></img>
</div>

  if(topProds!=undefined){
        render=<Carousel activeIndex={index} onSelect={handleSelect} className="text-center bg-dark">
        <Carousel.Item>
        <img
            style={{height:"200px"}}
            src={topProds[0].imgurl}
            alt="First slide"
        />
        <div style={{height:"150px"}}></div>
        <Carousel.Caption>
            <h4 className="my-0"><a href="" onClick={()=>navigate(`/productPage?id=${topProds[0]._id}`)}>{topProds[0].name}</a></h4>
            <p>{topProds[0].description}.</p>
            <p>{topProds[0].buycount} have bought.</p>
        </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <img
            style={{height:"200px"}}
            src={topProds[1].imgurl}
            alt="Second slide"
        />
            <div style={{height:"150px"}}></div>
        <Carousel.Caption>
        <h4 className="my-0"><a href="" onClick={()=>navigate(`/productPage?id=${topProds[0]._id}`)}>{topProds[1].name}</a></h4>
        <p>{topProds[1].description}.</p>
            <p>{topProds[1].buycount} have bought.</p>
        </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
            <img
            style={{height:"200px"}}
            src={topProds[2].imgurl}
            alt="Third slide"
        />
            <div style={{height:"150px"}}></div>
        <Carousel.Caption>
        <h4 className="my-0"><a href="" onClick={()=>navigate(`/productPage?id=${topProds[0]._id}`)}>{topProds[2].name}</a></h4>
        <p>{topProds[2].description}.</p>
            <p>{topProds[2].buycount} have bought.</p>
        </Carousel.Caption>
        </Carousel.Item>
    </Carousel>
  }
    return render
  }